package week7.assignment.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import week7.assignment.database.UserDatabase;
import week7.assignment.model.User;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super(); 
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		
		String regex = "^(.+)@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		
		if(matcher.matches()) {
			User user = new User(email,name,password);
			UserDatabase userDatabase = new UserDatabase();
			try {
				boolean isRegistered = userDatabase.insertValues(user);
				if(isRegistered) {
//					RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
//					dispatcher.forward(request, response);
					response.sendRedirect("login.jsp");
				}else {
					RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
					request.setAttribute("error","You Are Already Registered Please Login");
					dispatcher.forward(request, response);
				}
			}catch (SQLException e) {
				RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute("error","Error Connecting to Database! Try Agian After Sometime");
				dispatcher.forward(request, response);
			}
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
			request.setAttribute("error","Email Pattern does not match");
			dispatcher.forward(request, response);
		}
	}

}
