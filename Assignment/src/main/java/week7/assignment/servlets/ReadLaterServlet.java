package week7.assignment.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import week7.assignment.database.ReadLaterDatabase;
import week7.assignment.model.ReadLater;

@WebServlet("/read")
public class ReadLaterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ReadLaterServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ReadLaterDatabase readLater = new ReadLaterDatabase();

		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");

		if(email != null) {
			try {
				List<ReadLater> readLaterbooks = readLater.getAllReadLaterBooks(email);
				if(!readLaterbooks.isEmpty()) {
					RequestDispatcher dispatcher = request.getRequestDispatcher("readlater.jsp");
					request.setAttribute("listofreadLaterbooks", readLaterbooks);
					dispatcher.forward(request,response);
				}else {
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else {
			response.sendRedirect("login.jsp");
		}
	}
}
