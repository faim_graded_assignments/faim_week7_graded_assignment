package week7.assignment.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import week7.assignment.database.ReadLaterDatabase;

@WebServlet("/addread")
public class AddToReadLaterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddToReadLaterServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		ReadLaterDatabase readLaterDatabase = new ReadLaterDatabase();
		
		HttpSession httpSession = request.getSession();
		String email = (String)httpSession.getAttribute("email");
		
		boolean isInserted = false;
		try {
			boolean isPresent = readLaterDatabase.checkEntryInReadLaterTable(id);
			if(isPresent) {
				String error = "Book Already Present in Read Later Table";
				request.setAttribute("error",error);
			}else {
				isInserted = readLaterDatabase.addToReadLaterBook(id,email);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(isInserted) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("dashboard.jsp");
			dispatcher.forward(request, response);
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
			dispatcher.forward(request, response);
		}
		
	}

}
