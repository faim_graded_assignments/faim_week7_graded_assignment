package week7.assignment.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import week7.assignment.database.BookDatabase;
import week7.assignment.model.Book;

@WebServlet("/book")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BookServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		BookDatabase bookDatabase = new BookDatabase();
		try {
			List<Book> books = bookDatabase.getAllBooks();
			RequestDispatcher dispatcher = request.getRequestDispatcher("book.jsp");
			request.setAttribute("listOfBook", books);
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
