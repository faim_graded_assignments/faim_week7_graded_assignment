package week7.assignment.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import week7.assignment.database.UserDatabase;
import week7.assignment.model.User;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		HttpSession session = request.getSession();
		session.setAttribute("email",email);
		
		String regex = "^(.+)@(.+)$";
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		
		if(matcher.matches()) {
			User user = new User();
			user.setEmail(email);
			user.setPassword(password);
			
			UserDatabase userDatabase = new UserDatabase();
			try {
				if(userDatabase.validate(user)) {
					RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
					requestDispatcher.forward(request,response);
				}else {
					RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
					request.setAttribute("error","You are not a Registered User, Please Registered Yourself");
					dispatcher.forward(request, response);
				}
			} catch (SQLException e) {
				RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
				request.setAttribute("error","Error in Database Connection Please Try Again");
				dispatcher.forward(request, response);
			}
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("error.jsp");
			request.setAttribute("error","Email Pattern does not match");
			dispatcher.forward(request, response);
		}
	}
}
