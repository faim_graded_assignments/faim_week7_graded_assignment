package week7.assignment.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import week7.assignment.connection.DBConnection;
import week7.assignment.model.Liked;

public class LikedDatabase {

	private Connection conn = DBConnection.getConnection();

	public List<Liked> getAllLikedBooks(String email) throws SQLException {
		List<Liked> listOfLikedBooks = new ArrayList<>();

		String sql = "select * from likedbooks where email = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1,email);
		
		ResultSet resultSet = statement.executeQuery();
		while(resultSet.next()) {
			
			Liked liked = new Liked();
			liked.setBookId(resultSet.getInt(1));
			liked.setBookName(resultSet.getString(2));
			liked.setBookGenre(resultSet.getString(3));
			listOfLikedBooks.add(liked);
		}
		return listOfLikedBooks;
	}
	
	public boolean checkLikedTable(int id) throws SQLException {
		
		String sql = "select * from likedbooks where bookId = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1,id);
		ResultSet resultSet = statement.executeQuery();
		
		if(resultSet.next()) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean addToLikedBook(int id,String email) throws SQLException {
		String sql = "select * from book where bookId = ?";
		String insertSql = "insert into likedbooks values(?,?,?,?)";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1,id);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			statement = conn.prepareStatement(insertSql);
			
			statement.setInt(1,resultSet.getInt(1));
			statement.setString(2,resultSet.getString(2));
			statement.setString(3,resultSet.getString(3));
			statement.setString(4,email);
			statement.executeUpdate();
			return true;
		}else {
			return false;
		}
	}
}
