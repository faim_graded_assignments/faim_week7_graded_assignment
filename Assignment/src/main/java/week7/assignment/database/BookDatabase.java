package week7.assignment.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import week7.assignment.connection.DBConnection;
import week7.assignment.model.Book;

public class BookDatabase {
	
	private Connection conn = DBConnection.getConnection();
	
	public List<Book> getAllBooks() throws SQLException {
		
		List<Book> listOfAllBooks = new ArrayList<>();
		String sql = "select * from book;";
		Statement statement = conn.createStatement();
		
		ResultSet resultSet = statement.executeQuery(sql);
		while(resultSet.next()) {
			Book book = new Book();
			book.setBookId(resultSet.getInt(1));
			book.setBookName(resultSet.getString(2));
			book.setBookGenre(resultSet.getString(3));
			listOfAllBooks.add(book);
		}
		return listOfAllBooks;
	}
}
