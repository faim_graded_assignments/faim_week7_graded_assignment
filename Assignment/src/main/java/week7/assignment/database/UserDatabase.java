package week7.assignment.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import week7.assignment.connection.DBConnection;
import week7.assignment.model.User;

public class UserDatabase {
	
	private Connection conn = DBConnection.getConnection();
	
	public boolean validate(User user) throws SQLException {
		String sql = "select * from user where email= ?;";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1,user.getEmail());
		
		ResultSet resultSet = statement.executeQuery();
		
		if(resultSet.next()) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean insertValues(User user) throws SQLException {
		String select = "select * from user where email = ?";
		
		PreparedStatement statement = conn.prepareStatement(select);
		System.out.println(user.getEmail());
		statement.setString(1,user.getEmail());
		
		ResultSet resultSet = statement.executeQuery();
		
		String sql = "insert into user values(?,?,?)";
		if(!resultSet.next()) {
			
			statement = conn.prepareStatement(sql);
			statement.setString(1,user.getEmail());
			statement.setString(2,user.getName());
			statement.setString(3,user.getPassword());
			statement.executeUpdate();
			
			return true;
		}else {
			return false;
		}
	}
}
