package week7.assignment.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import week7.assignment.connection.DBConnection;
import week7.assignment.model.ReadLater;

public class ReadLaterDatabase {
	
	private Connection conn = DBConnection.getConnection();

	public List<ReadLater> getAllReadLaterBooks(String email) throws SQLException {

		List<ReadLater> listOfReadLaterBooks = new ArrayList<>();

		String sql = "select * from readlater where email = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1,email);
		ResultSet resultSet = statement.executeQuery();
		
		while(resultSet.next()) {
			
			ReadLater readLater = new ReadLater();
			readLater.setBookId(resultSet.getInt(1));
			readLater.setBookName(resultSet.getString(2));
			readLater.setBookGenre(resultSet.getString(3));
			
			listOfReadLaterBooks.add(readLater);
			
		}
		System.out.println(listOfReadLaterBooks);
		return listOfReadLaterBooks;
	}
	
	public boolean checkEntryInReadLaterTable(int id) throws SQLException {
		
		String sql = "select * from readlater where bookId = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		System.out.println(id);
		statement.setInt(1,id);
		ResultSet resultSet = statement.executeQuery();
		
		if(resultSet.next()) {
			return true;
		}else {
			return false;
		}
	}

	public boolean addToReadLaterBook(int id,String email) throws SQLException {
		String sql = "select * from book where bookId = ?";
		String insertSql = "insert into readlater values(?,?,?,?)";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1,id);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			statement = conn.prepareStatement(insertSql);
			
			statement.setInt(1,resultSet.getInt(1));
			statement.setString(2,resultSet.getString(2));
			statement.setString(3,resultSet.getString(3));
			statement.setString(4,email);
			statement.executeUpdate();
			
			return true;
		}else {
			return false;
		}
	}

}
