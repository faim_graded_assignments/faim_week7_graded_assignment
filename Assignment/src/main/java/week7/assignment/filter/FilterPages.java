package week7.assignment.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns = "/*")
public class FilterPages implements Filter {

    public FilterPages() {
        System.out.println("Filter Constructor");
    }
	public void destroy() {
		System.out.println("Filter destroy");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse)response;
		httpServletResponse.setHeader("Cache-Control","no-cache,no-store,");
		httpServletResponse.setHeader("Pragma","no-cache");
		httpServletResponse.setDateHeader("Expires", 0);
		
		chain.doFilter(httpServletRequest, httpServletResponse);
		
	}

	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("init filter");
	}

}
