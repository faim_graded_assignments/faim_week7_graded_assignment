create table user(
email varchar(25) primary key,
name varchar(20) not null,
password varchar(25) not null
);

create table book(
bookId int(10) primary key,
bookName varchar(25) not null,
bookGenre varchar() not null
);

insert inot book values
(1,'Phool','Thriller');
(2,'Nirja','Horror');
(3,'Gillu','Suspense');
(4,'Yama','Suspense');
(5,'Godan','Novel');
(6,'Nirmala','Novel');
(7,'Gaban','Novel');
(8,'Karamabhoomi','Novel');

create table liked(
bookId int(10) primary key,
bookName varchar(25) not null,
bookGenre varchar() not null,
email varchar(25) not null
foreign key (email) references user(email)
);

create table readlater(
bookId int(10) primary key,
bookName varchar(25) not null,
bookGenre varchar() not null,
email varchar(25) not null
foreign key (email) references user(email)
);

